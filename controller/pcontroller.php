<?php
include '../model/dbconn.php';

if(isset($_POST['addPerson'])){
    $fname=htmlentities($_POST['fname']);
    $lname=htmlentities($_POST['lname']);
    $age=htmlentities($_POST['age']);
    $gender=htmlentities($_POST['gender']);
    $bday=htmlentities($_POST['bday']);
    $password=htmlentities($_POST['password']);

    // $fname="EJ Anton";
    // $lname="POTOT";
    // $age="20";
    // $gender="Male";
    // $bday="4-14-1998";


    $data=array($fname,$lname,$age,$gender,$bday,$password);
    insertRecord($data);
    header("location:../view/person.php");
}
if(isset($_POST['updatePerson'])){
    $fname=htmlentities($_POST['fname']);
    $lname=htmlentities($_POST['lname']);
    $age=htmlentities($_POST['age']);
    $gender=htmlentities($_POST['gender']);
    $bday=htmlentities($_POST['bday']);
    $password=htmlentities($_POST['password']);
    $id=$_GET['id'];
    // $fname="Alyn";
    // $lname="POTOT";
    // $age=19;
    // $gender="Female";
    // $bday="1998-9-30";
    // $password="123";
    // $id=2;
    $data=array($fname,$lname,$age,$gender,$bday,$password,$id);
    updateRecord($data);
    header("location:../view/person.php");
}