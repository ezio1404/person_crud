<?php
session_start();
function dbconn(){
    try{
        return new PDO("mysql:hostname=localhost;dbname=sample_db","root","");
    }catch(PDOExecption $e){
        echo $e->getMessage();
    } 
}//end dbconn()
function destroy(){
    return null;
}

function logInUser($data){
    $dbconn=dbconn();
    $flag=false;
    $sql="SELECT * FROM tbl_person WHERE p_id=? AND p_password=?";
    $stmt=$dbconn->prepare($sql);
    $stmt->execute($data);
    $user=$stmt->fetch(PDO::FETCH_ASSOC);
    if($stmt->rowCount() > 0){
        $_SESSION['id']=$user['p_id'];
        $_SESSION['info']=$user['p_lname'].",".$user['p_fname'];
        $flag=true;
    }
    else{
        echo "<script> alert('Error') </script>";
    }
    $dbconn=destroy();
    return $flag;

}

function insertRecord($data){
    $dbconn=dbconn();
    $sql="INSERT INTO tbl_person(p_fname,p_lname,p_age,p_gender,p_bday,p_password) VALUES(?,?,?,?,?,?)";
    $stmt=$dbconn->prepare($sql);
    $stmt->execute($data);
    $dbconn=destroy();
}

function getAllRecord(){
    $dbconn=dbconn();
    $sql="SELECT * FROM tbl_person";
    $stmt=$dbconn->prepare($sql);
    $stmt->execute();
    $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $dbconn=destroy();
    return $rows;
}

function getRecord($data){
    $dbconn=dbconn();
    $sql="SELECT * FROM tbl_person where p_id=?";
    $stmt=$dbconn->prepare($sql);
    $stmt->execute($data);
    $row = $stmt->fetch();
    $dbconn=destroy();
    return $row;
}

function updateRecord($data){
    $dbconn=dbconn();
    $sql="UPDATE tbl_person SET p_fname=?,p_lname=?,p_age=?,p_gender=?,p_bday=? ,p_password=?WHERE p_id=?";
    $stmt=$dbconn->prepare($sql);
    $stmt->execute($data);
    $dbconn=destroy();
}

function deleteRecord($data){
    $dbconn=dbconn();
    $sql="DELETE FROM tbl_person WHERE p_id=?";
    $stmt=$dbconn->prepare($sql);
    $stmt->execute($data);
    $dbconn=destroy();
}






