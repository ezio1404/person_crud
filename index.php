<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="view/assets/css/bootstrap.css">
    <title>Simple</title>
</head>
<body>
    <div>
        <form action="controller/logController.php" method="POST">
            <input type="text" name="id" id="id" placeholder="ID">
            <input type="password" name="pass" id="pass" placeholder="*******">
            <input type="submit" value="Login" name="login">
        </form>
    </div>
    <script src="view/assets/js/jquery.js"></script>
    <script src="view/assets/js/bootstrap.js"></script>
</body>
</html>